---
title: "Graduate Researcher - SHARP Project"
summary: GRA Position in the FTL Lab
#date: "2018-06-28T00:00:00Z"

reading_time: false  # Show estimated reading time?
share: true  # Show social sharing links?
profile: false  # Show author profile?
comments: false  # Show comments?

# Optional header image (relative to `static/media/` folder).
header:
  caption: ""
  image: ""
---

We are seeking candidates for the SHARP and Blink projects
([read more here](/projects/sharp)
[and here](/projects/blink)
).

These projects require knowledge of compiler design, digital hardware
design, DSP system design, computer architecture, and memory-sharing
schemes. The Blink project requires familiariy of contemporary AI/ML network architectures and training algorithms.


We prefer students who are pursuing the Ph.D. though M.S. students
will be considered.

Absolutely critical is competency with the python (and C++) language, th>
"git" version control, and some basic intuition of algorithmic design an>

Most of all, we are looking for bright students who are curious 
and willing (and able) to self-direct the resolution of low-level
problems (google is your friend) while helping to create a rich,
stimulating, and fun environment for our high-level pursuits.

## Desired Skills:
 - python
 - FPGA programming experience
 - Verilog experience
 - C++ development experience
 - CMOS design experience

## Application Materials (email address provided on request):
 - One page explaining your interests within ECE and how they overlap wi>
 - resume/cv
 - copy of your academic transcripts

## Application Instructions:
Use the ["Contact" form](/contact) to let us know you're interested, and we will
provide an email address for you to send your materials.
Alternatively, if you can find Dr. Muldrey's email address,
you may just send him an email directly containing the materials.

## [Send us a message to inquire!](/contact)
