---
title: "Graduate Researcher - High-level Analog Synthesis"
summary: GRA Position in the FTL Lab
#date: "2018-06-28T00:00:00Z"
share: true  # Show social sharing links?
profile: false  # Show author profile?
comments: false  # Show comments?

# Optional header image (relative to `static/media/` folder).
header:
  caption: ""
  image: ""
---

We are seeking candidates for the Hilas and Blink projects
([read more here](/projects/hilas)
[and here](/projects/blink)).

These projects require familiarity with analog ciruit design and
CMOS device physics. Students should have a deep
understanding of the VLSI design "stack" including Verilog
programming, circuit synthesis, chip floorplanning, place-and-route,
and parasitics extraction.

We prefer students who are pursuing the Ph.D. though M.S. students
will be considered.

Absolutely critical is competency with the python (and C++) language, the use of
"git" version control, and some basic intuition of algorithmic design and analysis.

Most of all, we are looking for bright students who are curious
and willing (and able) to self-direct the resolution of low-level
problems (google is your friend) while helping to create a rich,
stimulating, and fun environment for our high-level pursuits.


## Desired Skills:
 - strong knowledge of analog circuit design
 - strong command of *nix system programming
 - understanding of optimization problems
 - familiarity with Git
 - experience using optimization solvers
 - must enjoy programming and tool dev!

## Application Materials (email address provided on request):
 - One page explaining your interests within ECE and how they overlap with this project
 - resume/cv
 - copy of your academic transcripts

## Application Instructions:
Use the ["Contact" form](/contact) to let us know you're interested, and>
provide an email address for you to send your materials.
Alternatively, if you can find Dr. Muldrey's email address,
you may just send him an email directly containing the materials.

## [Send us a message to inquire!](/contact)
