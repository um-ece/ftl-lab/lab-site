---
title: "Hilas: High Level Analog Synthesis"
summary: Overcoming barriers to VLSI-style design flows for analog systems.
tags:
- analog
- synthesys
- NLP

#date: "2016-04-27T00:00:00Z"

# Optional external URL for project (replaces project detail page).
#external_link: ""

image:
  caption: "(c)3D-IP"
  focal_point: Smart

links:
- icon: gitlab
  icon_pack: fab
  name: "Source Code"
  url: "https://gitlab.com/um-ece/ftl-lab/hilas"

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
#slides: example

---

### Summary
The Hilas project is a collaboration with Prof. Jennifer Hasler 
(Ga. Tech) and Dr. Sahil Shah (Maryland) which is exploring
techniques for automated synthesis of analog circuits at the layout
level.

### Background
For the last several years, Dr. Hasler's lab has made developments
in the production of FPAAs (field-programmable *analog* arrays).
They have fabricated several generations of the device now, and
have a fertile ecosystem of tools for designing and programming 
analog systems onto the devices. The lab has cultivated expertise
at the device and layout level through their work.

In parallel, there has been an effort from the *digital* VLSI
community to create opensource tools to free designers (and their
employers) from the lock-in that results from the use of vendor-
specific tools, like Cadence Virtuoso. The OpenROAD and OpenLANE
projects have recently made great progress in the release of 
a viable toolchain to take designs from Verilog to layout.

Our lab is situated between these two thrusts. Our expertise comes
from experience in the modeling of AMS systems and integration with
an array of AMS EDA tools. 

### Present Work
Our current effort is focused on achiving autonomous reproduction
of layout level descriptions of designs which are provided by
Dr. Hasler's lab.
