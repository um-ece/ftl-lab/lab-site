---
title: "SHARP: Spontaneous Hardware Accelleration and Resource Planning"
summary: "Approaches for leveraging Heterogenous CPU/GPU/FPGA/FPAA Computing Systems in Novel Ways"
tags:
- Synthesis
- Decompilation
- Intent Estimation

# Optional external URL for project (replaces project detail page).
#external_link: ""

image:
  caption: ""
  focal_point: Smart

links:
    - icon: gitlab
      icon_pack: fab
      name: "Source Code"
      url: "https://gitlab.com/um-ece/ftl-lab/sharp"
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
#slides: example
---
### Summary
The SHARP project is a collaboration with Dr. Michael Giardino of the
[Systems Group at ETH](https://systems.ethz.ch) in Zurich, Switzerland.
The project works to formulate novel techniques that would allow 
a supervisory application to sample the runtime behavior of a 
user application and spontaneously create and deploy FPGA-based
accelerators to increase user application performance.

The main thrusts are:
 1. Unsupervised synthesis of FPGA accelerators from (source, binary)
 2. Selective deployment of individual accelerators subject to various
optimization constriaints and objectives.
 3. On-line integration of accelerators into executing applications

### Background
The Systems Group has recently completed the design of their Enzian
System which is a novel, shared-memory heterogenous architecture.
Soon, Enzian systems will be made available to researchers around the 
world.

Simultaneously, FPGA accellerators are starting to appear in data-
centers in a hardware-as-a-service (HaaS) manner. It is currently
time-consuming and difficult for application software designers
to take on the additional burden of designing FPGA-based accelerators.

### Present Work
Currently, we are experimenting on the Xilinx Zynq platform which
combines an ARM GPCPU core with a Xilinx UltraScale+ FPGA fabric.
