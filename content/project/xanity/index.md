---
title: Xanity Platform
summary: An experimental framework to simplify execution and data management across languages, computers, researchers.
tags:
- Hyperparameter Optimization
- Data Flow Graphs
- CAD tools
- Design of Experiments

# Optional external URL for project (replaces project detail page).
#external_link: ""

image:
  caption: ""
  focal_point: Smart

links:
#- icon: twitter
#  icon_pack: fab
#  name: Follow
#  url: https://twitter.com/georgecushen
- icon: gitlab
  icon_pack: fab
  name: Source
  url: https://gitlab.com/bjmuld/xanity
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
#slides: example
---

## Summary
Xanity is a software framework for computing researchers. 
Xanity maintains a dependency graph of all the experiments, data,
 and analyses in a project, and will automatically trigger only
 necessary computation when a run is launched.
It eases the burden of experiment management, data labeling,
trial labelling, etc.
It keeps track of which version of code was responsible for which results;
it keeps track of successful and unsuccessful runs;
it keeps track of individual subexperiments;
and
it provides an easy interface for data management.


## Background

Xanity is the accumulation of various helpful boilerplate code 
fragments that have been used over the years.

## Present Work
Promotion! Bug Fixes, improvements, and maintenance.
