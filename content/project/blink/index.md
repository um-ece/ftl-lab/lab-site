---
title: "Blink: Binary Lightweight Neural Kit"
summary: "Building minimally parameterized boolean networks"
tags:
    - deep learning
    - artificial intelligence
    - machine learning
    - neuromorphics
date: "2020-10-28T10:29:00Z"
image:
    caption: ""
    focal_point: Smart

links:
    - icon: gitlab
      icon_pack: fab
      name: "Source Code"
      url: "https://gitlab.com/um-ece/ftl-lab/blink"

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

### Summary
The Blink project seeks to exploit the conjecture that contemporary
deep-learning networks are highly overparameterized and overly large.
The process of searching this bloated space for suitable weight values is
extremely carbon-intense (petroleum, natural-gas, etc.) and is highly
inefficient.
Other researchers have shown that, once trained, networks can be
shrunken (pruned) by a substantial amount (60-80%) while retaining
their performance. 
We are researching ways of constructing networks in a more intelligent
and resource-efficient manner from the beginning.

### Background
This work is primariliy inspired by the ["Lottery Ticket Hypothesis" paper
by J. Frankle and M. Carbin](https://arxiv.org/abs/1803.03635) and by
the work of Bhavya Kailkhura (L.L.N.L.).

