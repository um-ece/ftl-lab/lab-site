+++
# Hero widget.
widget = "hero"  # See https://wowchemy.com/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 10  # Order that this section will appear.

title = "FTL is hiring!"

# Hero image (optional). Enter filename of an image in the `static/media/` folder.
#hero_media = "hero-academic.png"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  #color = "red"
  
  # Background gradient.
  gradient_start = "#40b4e3"
  gradient_end = "#1074c3"
  
  # Background image.
  # image = ""  # Name of image in `static/media/`.
  # image_darken = 0.6  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.
  # image_size = "cover"  #  Options are `cover` (default), `contain`, or `actual` size.
  # image_position = "center"  # Options include `left`, `center` (default), or `right`.
  # image_parallax = true  # Use a fun parallax-like fixed background effect? true/false
  
  # Text color (true=light or false=dark).
  text_color_light = true

# Call to action links (optional).
#   Display link(s) by specifying a URL and label below. Icon is optional for `[cta]`.
#   Remove a link/note by deleting a cta/note block.

[cta]
  url = "/openings"
  label = "View Openings"
#  icon_pack = "fas"
  #icon = "user-ninja"
  
#[cta_alt]
#  url = "/openings"
#  label = "Read About Openings"

# Note. An optional note to show underneath the links.
#[cta_note]
#  label = '<a class="js-github-release" href="https://wowchemy.com/updates" data-repo="wowchemy/wowchemy-hugo-modules">Latest release<!-- V --></a>'
+++

Please have a look and send a message if you're curious. You can read 
more about the active projects on the [projects](project/) page
and more about what we're looking for on the [openings](openings/) page.

We look forward to hearing from you!
