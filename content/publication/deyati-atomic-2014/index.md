---
# Documentation: https://wowchemy.com/docs/managing-content/

title: 'Atomic Model Learning: A Machine Learning Paradigm for Post Silicon Debug
  of RF/Analog Circuits'
subtitle: ''
summary: ''
authors:
- S. Deyati
- B. J. Muldrey
- A. Banerjee
- A. Chatterjee
tags:
- '"analog circuits"'
- '"analogue integrated circuits"'
- '"Artificial neural networks"'
- '"atomic model learning"'
- '"design bug diagnosis"'
- '"electronic engineering computing"'
- '"Fault diagnosis"'
- '"integrated circuit design"'
- '"Integrated circuit modeling"'
- '"integrated circuit modelling"'
- '"learning (artificial intelligence)"'
- '"machine learning paradigm"'
- '"Mathematical model"'
- '"mixed-signal SoC"'
- '"Mixers"'
- '"Model checking"'
- '"module level behavior"'
- '"post silicon debug"'
- '"Power amplifiers"'
- '"Preamplifiers"'
- '"Radio frequency"'
- '"Radio transmitters"'
- '"radiofrequency integrated circuits"'
- '"RF circuits"'
- '"RF SoC"'
- '"supervised learning"'
- '"Tuning"'
categories: []
date: '2014-04-01'
lastmod: 2020-10-15T20:25:26-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-10-16T01:25:26.057003Z'
publication_types:
- '1'
abstract: As RF design scales to the 28nm technology node and beyond, pre-silicon
  simulation and verification of complex mixed-signal/RF SoCs is becoming intractable
  due to the difficulties associated with simulating diverse electrical effects and
  design bugs. As a consequence, there is increasing pressure to develop comprehensive
  post-silicon test and debug tools that can be used to identify design bugs and improve
  modeling of complex electrical nonidealities observed in silicon. Often, it is not
  known a-priori what these bugs are and how they can be modeled, significantly complicating
  the debug process. In this research, a new atomic model learning approach is proposed
  that uses supervised learning techniques to diagnose design bugs and learn unknown
  module-level behaviors. Nonideality modeling artifacts called model atoms are inserted
  into different nodes of the design signal flow paths to learn unknown behaviors
  along those paths. Under the assumption that the design bug is localized, it is
  shown that the source of the bug can be identified with high resolution even when
  the nature of the bug is unknown. The method has been applied to a conventional
  wireless as well as a polar radio transmitter and key results that demonstrate usefulness
  and feasibility of the proposed approach are presented.
publication: '*2014 IEEE 32nd VLSI Test Symposium (VTS)*'
doi: 10.1109/VTS.2014.6818791
---
