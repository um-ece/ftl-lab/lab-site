---
# Documentation: https://wowchemy.com/docs/managing-content/

title: 'TRAP: Test Generation Driven Classification of Analog/RF ICs Using Adaptive
  Probabilistic Clustering Algorithm'
subtitle: ''
summary: ''
authors:
- S. Deyati
- B. J. Muldrey
- A. Chatterjee
tags:
- '"adaptive probabilistic clustering algorithm"'
- '"analog IC"'
- '"analogue integrated circuits"'
- '"Automatic test pattern generation"'
- '"Circuit testing"'
- '"Classification algorithms"'
- '"Integrated circuit modeling"'
- '"integrated circuit testing"'
- '"Mathematical model"'
- '"probabilistic neural networks"'
- '"probability"'
- '"production testing"'
- '"radiofrequency integrated circuits"'
- '"RF IC"'
- '"Standards"'
- '"test generation driven classification"'
- '"testing"'
- '"Training"'
- '"TRAP"'
categories: []
date: '2016-01-01'
lastmod: 2020-10-15T20:25:26-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-10-16T01:25:26.676269Z'
publication_types:
- '1'
abstract: In production testing of analog/RF ICs, application of standard specification-based
  tests for IC classification is not always an attractive option due to the high costs
  and test times involved. In this paper, a new test generation algorithm for IC classification
  is first developed that has the property that the corresponding DUT response signatures
  for devices from diverse process corners are maximally separable. In other words,
  the process space can be partitioned into a maximally large number of partitions
  and each tested IC can be uniquely diagnosed to lie in one such partition from its
  response to the applied test stimulus. Boolean zed representations of analog/RF
  circuits are used to facilitate rapid test stimulus generation. Next, the responses
  of ICs tested in production to the applied classification test are used to adaptively
  classify the ICs into clusters of \"good\", \"bad\" and \"marginal\" devices (TRAP).
  This is done through the use of probabilistic neural networks that do not require
  complete network retraining every time an IC with different input-output behavior
  is observed. The classification test applied helps in determining if a new device
  being tested is from a different process corner than encountered before, thereby
  aiding IC classification. Simulation experiments show that the learning process
  is rapid and minimizes device misclassification with significant savings in test
  time.
publication: '*2016 29th International Conference on VLSI Design and 2016 15th International
  Conference on Embedded Systems (VLSID)*'
doi: 10.1109/VLSID.2016.118
---
