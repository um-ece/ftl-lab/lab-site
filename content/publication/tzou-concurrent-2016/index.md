---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Concurrent Multi-Channel Crosstalk Jitter Characterization Using Coprime Period
  Channel Stimulus
subtitle: ''
summary: ''
authors:
- Nicholas L. Tzou
- Debesh Bhatta
- Xian Wang
- Te-Hui Chen
- Sen-Wen Hsiao
- Barry Muldrey
- Hyun Woo Choi
- Abhijit Chatterjee
tags:
- '"back-end digital signal processing"'
- '"bit error rate"'
- '"Bit error rate"'
- '"bounded uncorrelated jitter"'
- '"Bounded uncorrelated jitter"'
- '"channel models"'
- '"circuits"'
- '"components"'
- '"components; circuits; devices and systems"'
- '"concurrent multichannel crosstalk jitter characterization"'
- '"coprime bit sequence"'
- '"coprime lengths"'
- '"coprime period channel stimulus"'
- '"coprime periods"'
- '"couplings"'
- '"crosstalk"'
- '"crosstalk components"'
- '"crosstalk coupling"'
- '"crosstalk jitter"'
- '"data handling"'
- '"devices and systems"'
- '"hardware measurements"'
- '"high-speed data links"'
- '"histograms"'
- '"Histograms"'
- '"jitter"'
- '"jitter separation"'
- '"mathematical analysis"'
- '"multiple crosstalk jitter effects"'
- '"Noise measurement"'
- '"reduced device dimensions"'
- '"repetitive data patterns"'
- '"sampling methods"'
- '"single data"'
- '"subNyquist sampling"'
- '"time domain"'
- '"time-domain analysis"'
- '"unbiased crosstalk characterization"'
categories: []
date: '2016-01-01'
lastmod: 2020-10-15T20:25:29-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-10-16T01:25:29.052668Z'
publication_types:
- '2'
abstract: In recent years, the use of highly parallelized and high-speed data links
  has exacerbated the problem of crosstalk coupling. Signals with high frequency components
  couple to and degrade the quality of signals in adjacent lines aggressively with
  reduced device dimensions. In this paper, we propose a methodology for characterizing
  multiple crosstalk jitter effects in the time domain using a single data capture
  without the use of any channel models. The method uses repetitive data patterns
  across different signal lines with coprime periods. Each signal with crosstalk effects
  is digitized using sub-Nyquist sampling, and after back-end digital signal processing,
  the original transmitted signal on each line is isolated from its crosstalk components.
  Mathematical analysis shows that only by using patterns with coprime lengths, unbiased
  crosstalk characterization is possible. Hardware measurements support the proposed
  test methodology.
publication: '*Circuits and Systems I: Regular Papers, IEEE Transactions on*'
doi: 10.1109/TCSI.2016.2537898
---
