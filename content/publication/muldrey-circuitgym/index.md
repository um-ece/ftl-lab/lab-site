---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Circuitgym
subtitle: ''
summary: ''
authors:
- Barry Muldrey
tags: []
categories: []
date: -01-01
lastmod: 2020-10-15T20:25:28-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-10-16T01:25:28.064206Z'
publication_types:
- '0'
abstract: OpenAI-gym -like environments for spice circuits
publication: ''
---
