---
# Documentation: https://wowchemy.com/docs/managing-content/

title: 'Self-Learning RF Receiver Systems: Process-Aware Real-Time Adaptation to Channel
  Conditions for Low Power Operation'
subtitle: ''
summary: ''
authors:
- Debashis Banerjee
- Barry Muldrey
- Xian Wang
- Shreyas Sen
- Abhijit Chatterjee
tags: []
categories: []
date: '2016-01-01'
lastmod: 2020-10-15T20:25:25-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-10-16T01:25:25.421093Z'
publication_types:
- '2'
abstract: Prior research has established that dynamically trading-off the performance
  of the radio-frequency (RF) front-end for reduced power consumption across changing
  channel conditions, using a feedback control system that modulates circuit and algorithmic
  level ``tuning knobs'' in real-time based on received signal quality, leads to significant
  power savings. It is also known that the optimal power control strategy depends
  on the process conditions corresponding to the RF devices concerned. This leads
  to an explosion in the search space needed to find the best feedback control strategy,
  across all combinations of channel conditions and receiver process corners, making
  simulation driven optimal control law design impractical and computationally infeasible.
  Since this problem is largely intractable due to the above complexity of simulation,
  we propose a self-learning strategy for adaptive RF systems. In this approach, RF
  devices learn their own performance vs. power consumption vs. tuning knob relationships
  ``on-the-fly'' and formulate the most power-optimal control strategy for real-time
  adaptation of the RF system using neural-network based learning techniques during
  real-time operation. The methodology is demonstrated using SISO & MIMO RF receiver
  front-ends as test vehicles and is supported by hardware validation leading to 2.5X-3X
  power savings with minimal overhead.
publication: '*IEEE Transactions on Circuits and Systems I: Fundamental Theory and
  Applications*'
---
