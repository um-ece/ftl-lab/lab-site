---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Design of Efficient Analog Physically Unclonable Functions Using Alternative
  Test Principles
subtitle: ''
summary: ''
authors:
- Sabyasachi Deyati
- Barry Muldrey
- Adit Singh
- Abhijit Chatterjee
tags: []
categories: []
date: '2017-01-01'
lastmod: 2020-10-15T20:25:27-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-10-16T01:25:27.291883Z'
publication_types:
- '1'
abstract: ''
publication: '*2017 International Mixed Signals Testing Workshop (IMSTW)*'
---
