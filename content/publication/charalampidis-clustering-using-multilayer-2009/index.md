---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Clustering Using Multilayer Perceptrons
subtitle: ''
summary: ''
authors:
- Dimitrios Charalampidis
- Barry Muldrey
tags:
- '"68t99"'
- '"Clustering"'
- '"Multilayer Perceptrons"'
- '"Neural Networks"'
categories: []
date: '2009-01-01'
lastmod: 2020-10-15T20:25:25-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-10-16T01:25:25.621269Z'
publication_types:
- '2'
abstract: In this paper we present a multilayer perceptron-based approach for data
  clustering. Traditionally, data clustering is performed using either exemplar-based
  methods that employ some form of similarity or distance measure, discriminatory
  function-based methods that attempt to identify one or several cluster-dividing
  hyper-surfaces, point-by-point associative methods that attempt to form groups of
  points in a pyramidal manner by directly examining the proximity between pairs of
  points or groups of points, and probabilistic methods which assume that data are
  sampled from mixture distributions. Commonly, in exemplar-based methods, each cluster
  is represented by a multi-dimensional centroid. In this paper, we explore the function
  approximation capabilities of multilayer perceptron neural networks in order to
  build exemplars which are not simply points but curves or surfaces. The proposed
  technique aims to group data points into arbitrary-shaped point clouds. The proposed
  approach may exhibit problems similar to other traditional exemplar-based clustering
  techniques such as k -means, including convergence to local minimum solutions with
  respect to the cost function. However, it is illustrated in this work that approaches
  such as split-and-merge can be appropriately adjusted and employed in the proposed
  technique, in order to alleviate the problem of reaching poor local minimum solutions.
publication: '*Nonlinear Analysis*'
doi: 10.1016/j.na.2009.06.064
---
