---
# Documentation: https://wowchemy.com/docs/managing-content/

title: 2015 JETTA-TTTC Best Paper Award
subtitle: ''
summary: ''
authors:
- Nicholas Tzou
- Debesh Bhatta
- Barry J. Muldrey Jr
- Thomas Moon
- Xian Wang
- Hyun Choi
- Abhijit Chatterjee
- Cost Sparse Multiband Signal
- Characterization Using Asynchronous Multi-Rate
tags: []
categories: []
date: '2016-01-01'
lastmod: 2020-10-15T20:25:29-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-10-16T01:25:29.186300Z'
publication_types:
- '2'
abstract: ''
publication: '*J Electron Test*'
---
