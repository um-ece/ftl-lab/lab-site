---
# Documentation: https://wowchemy.com/docs/managing-content/

title: 'Validation Signature Testing: A Methodology for Post-Silicon Validation of
  Analog/Mixed-Signal Circuits'
subtitle: ''
summary: ''
authors:
- A. Chatterjee
- S. Deyati
- B. Muldrey
- S. Devarakond
- A. Banerjee
tags:
- '"analog-mixed-signal circuits"'
- '"Circuit faults"'
- '"circuit optimisation"'
- '"circuits"'
- '"communication"'
- '"communication; networking and broadcast technologies"'
- '"Complexity theory"'
- '"components"'
- '"components; circuits; devices and systems"'
- '"computing and processing"'
- '"Conferences"'
- '"crosstalk"'
- '"device under test behaviour"'
- '"devices and systems"'
- '"digital circuitry"'
- '"digital systems"'
- '"DUT behavior"'
- '"electrical bugs"'
- '"elemental semiconductors"'
- '"ground planes"'
- '"Hardware design languages"'
- '"integrated circuit design"'
- '"Integrated circuit modeling"'
- '"integrated circuit testing"'
- '"mixed analogue-digital integrated circuits"'
- '"mixed-signal SoC"'
- '"multidimensional variability effects"'
- '"networking and broadcast technologies"'
- '"nonlinear optimization algorithms"'
- '"nonlinear programming"'
- '"Post-silicon validation"'
- '"Post-silicon Validation"'
- '"post-silicon validation methodology"'
- '"Radio frequency"'
- '"radiofrequency integrated circuits"'
- '"reliability"'
- '"RF devices"'
- '"RF SoC"'
- '"Si"'
- '"silicon"'
- '"size 45 nm"'
- '"System-on-a-chip"'
- '"system-on-chip"'
- '"validation signature testing"'
- '"verification"'
- '"yield loss debugging"'
categories: []
date: '2012-01-01'
lastmod: 2020-10-15T20:25:25-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-10-16T01:25:25.842585Z'
publication_types:
- '1'
abstract: Due to the use of scaled technologies, high levels of integration and high
  speeds of today's mixed-signal SoCs, the problem of validating correct operation
  of the SoC under electrical bugs and that of debugging yield loss due to unmodeled
  multi-dimensional variability effects is extremely challenging. Precise simulation
  of all electrical aspects of the design including the interfaces between digital
  and analog circuitry, coupling across power and ground planes, crosstalk, etc.,
  across all process corners is very hard to achieve in a practical sense. The problem
  is expected to get worse as analog/mixed-signal/RF devices scale beyond the 45nm
  node and are more tightly integrated with digital systems than at present. In this
  context, a post-silicon validation methodology for analog/mixed-signal/RF SoCs is
  proposed that relies on the use of special stimulus designed to expose differences
  between observed DUT behavior and its predictive model. The corresponding error
  signature is then used to identify the likely \"type\" of electrical bug and its
  location in the design using nonlinear optimization algorithms. Results of trial
  experiments on RF devices are presented.
publication: '*ACM*'
doi: 10.1145/2429384.2429504
---
