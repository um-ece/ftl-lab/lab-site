---
# Documentation: https://wowchemy.com/docs/managing-content/

title: 'Automated Cross-Level Validation and Debug of Mixed-Signal Systems in Top-Down
  Design: From Pre-Silicon to Post-Silicon GRC Task 2712.005'
subtitle: ''
summary: ''
authors:
- B Muldrey
- S Deyati
tags: []
categories: []
date: -01-01
lastmod: 2020-10-15T20:25:28-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-10-16T01:25:27.930667Z'
publication_types:
- '2'
abstract: ''
publication: ''
---
