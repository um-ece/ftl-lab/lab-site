---
# Documentation: https://wowchemy.com/docs/managing-content/

title: 'DE-LOC: Design Validation and Debugging under Limited Observation and Control,
  Pre- and Post-Silicon for Mixed-Signal Systems'
subtitle: ''
summary: ''
authors:
- B. Muldrey
- S. Deyati
- A. Chatterjee
tags:
- '"Algorithm design and analysis"'
- '"Analog"'
- '"Circuit faults"'
- '"Computer bugs"'
- '"Debugging"'
- '"Defect detection"'
- '"diagnosis"'
- '"fault detection"'
- '"mixed-signal"'
- '"Predictive models"'
- '"silicon"'
- '"testing"'
- '"Training"'
categories: []
date: '2016-11-01'
lastmod: 2020-10-15T20:25:28-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-10-16T01:25:28.273511Z'
publication_types:
- '1'
abstract: In the modern mixed-signal SoC design cycle, designers are frequently tasked
  with detecting and diagnosing behavioral discrepancies between design descriptions
  given at different levels of hierarchy, e.g. behavioral vs. transistor level descriptions
  or behavioral/transistor level descriptions vs. fabricated silicon. One problem
  is detection, to determine if behavioral differences between design descriptions
  exist. If such differences (anomalies) are detected, then diagnosis is concerned
  with identifying the module in a hierarchical design description of the system that
  is most likely the root cause of the anomaly (typically under the constraint that
  only the primary outputs of the top-level hierarchies are observed. Previously proposed
  machine-learning classifiers require prior knowledge about the kinds of likely design
  errors typically encountered. In this work, we present a novel technique for the
  algorithmic foundation of circuit diagnosis predictions which does not require any
  assumptions about the nature of design errors. Our method employs iterative and
  alternate on-the-fly test generation and least-squares fitting of embedded low-order
  nonlinear filters to produce a best-guess estimate of the root cause of the anomaly.
  Experiments are conducted on two test vehicles, an RF transceiver and a phase-locked
  loop, several bug models are implemented, and the system's diagnosis predictions
  are analyzed.
publication: '*2016 IEEE International Test Conference (ITC)*'
doi: 10.1109/TEST.2016.7805868
---
