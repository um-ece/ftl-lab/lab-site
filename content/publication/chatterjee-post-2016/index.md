---
# Documentation: https://wowchemy.com/docs/managing-content/

title: 'Post Silicon Validation of Analog/Mixed Signal/RF Circuits and Systems: Recent
  Advances'
subtitle: ''
summary: ''
authors:
- A. Chatterjee
- S. Deyati
- B. J. Muldrey
tags:
- '"analog circuits"'
- '"analogue integrated circuits"'
- '"bug detection"'
- '"bug diagnosis"'
- '"capacitive coupling"'
- '"Computer bugs"'
- '"Delays"'
- '"elemental semiconductors"'
- '"failure analysis"'
- '"failure mechanisms"'
- '"guided stochastic test stimulus generation"'
- '"Hardware"'
- '"incorrect biasing conditions"'
- '"incorrect guard-banding"'
- '"Integrated circuit modeling"'
- '"mixed analogue-digital integrated circuits"'
- '"mixed signal circuits"'
- '"Radio frequency"'
- '"radiofrequency circuits"'
- '"radiofrequency integrated circuits"'
- '"Si"'
- '"silicon"'
- '"silicon validation"'
- '"SoC"'
- '"system-on-chip"'
- '"technology scaling"'
- '"Voltage measurement"'
categories: []
date: '2016-07-01'
lastmod: 2020-10-15T20:25:25-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-10-16T01:25:25.730634Z'
publication_types:
- '1'
abstract: Technology scaling along with unprecedented levels of device integration
  has led to increasing numbers of analog/mixed-signal/RF design bugs escaping into
  silicon. Such bugs are manifested under specific system-on-chip (SoC) operating
  conditions and their effects are difficult to predict a-priori. This paper describes
  recent advances in detecting and diagnosing such bugs using \"guided\" stochastic
  test stimulus generation algorithms. A key challenge is that unlike traditional
  test generation for manufacturing test that is predicated on known failure mechanisms,
  the nature of design bugs is generally unknown and must be discovered on-the-fly.
  Classes of design errors from undesired capacitive coupling and incorrect biasing
  conditions to incorrect guard-banding of designs are considered. It is shown that
  high design bug coverage can be obtained over a range of test cases.
publication: '*2016 IEEE 21st International Mixed-Signal Testing Workshop (IMSTW)*'
doi: 10.1109/IMS3TW.2016.7524222
---
