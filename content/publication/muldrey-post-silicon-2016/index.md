---
# Documentation: https://wowchemy.com/docs/managing-content/

title: 'Post-Silicon Validation: Automatic Characterization of RF Device Nonidealities
  Via Iterative Learning Experiments on Hardware'
subtitle: ''
summary: ''
authors:
- Barry Muldrey
- Sabyasachi Deyati
- Abhijit Chatterjee
tags: []
categories: []
date: '2016-01-01'
lastmod: 2020-10-15T20:25:27-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-10-16T01:25:27.451951Z'
publication_types:
- '1'
abstract: Recent studies show that increasing numbers of design bugs are escaping
  to post-silicon due to the complexity of advanced designs and the lack of adequate
  verification tools that can validate complex electrical interactions between electrical
  subsystems on an integrated circuit. In this paper, we present a novel tool for
  post-silicon validation of mixed-signal/RF cir- cuits through cooperative test stimulus
  generation and behavior- learning. The implemented technique leverages iterative
  super- vised learning techniques to comprehensively diagnose anomalies between the
  input-output behavior of the silicon device and the corresponding behavior predicted
  by its reference model. This results in both a more efficient validation test stimulus
  and an improved behavioral model which captures non-ideal silicon behaviors. Preliminary
  results on RF devices prove the feasibility of the proposed validation methodology.
publication: '*VLSI Design Conference*'
---
