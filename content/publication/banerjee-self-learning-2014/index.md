---
# Documentation: https://wowchemy.com/docs/managing-content/

title: 'Self-Learning MIMO-RF Receiver Systems: Process Resilient Real-Time Adaptation
  to Channel Conditions for Low Power Operation'
subtitle: ''
summary: ''
authors:
- D. Banerjee
- B. Muldrey
- S. Sen
- X. Wang
- A. Chatterjee
tags:
- '"Adaptation"'
- '"Artificial neural network"'
- '"feedback control system"'
- '"learning (artificial intelligence)"'
- '"LNA"'
- '"Low Power"'
- '"low power operation"'
- '"MIMO"'
- '"MIMO communication"'
- '"Mixer"'
- '"neural-network based learning techniques"'
- '"OFDM"'
- '"optimal power control strategy"'
- '"Optimized production technology"'
- '"Power demand"'
- '"Radio frequency"'
- '"radio receivers"'
- '"Real-time systems"'
- '"Receiver. Radio-Frequency"'
- '"Receivers"'
- '"reduced power consumption"'
- '"resilient real-time adaptation"'
- '"RF devices"'
- '"Self-learning"'
- '"self-learning MIMO-RF receiver systems"'
- '"Tuning"'
categories: []
date: '2014-11-01'
lastmod: 2020-10-15T20:25:25-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-10-16T01:25:25.298805Z'
publication_types:
- '1'
abstract: Prior research has established that dynamically trading-off the performance
  of the RF front-end for reduced power consumption across changing channel conditions,
  using a feedback control system that modulates circuit and algorithmic level \"tuning
  knobs\" in real-time, leads to significant power savings. It is also known that
  the optimal power control strategy depends on the process conditions corresponding
  to the RF devices concerned. This complicates the problem of designing the feedback
  control system that guarantees the best control strategy for minimizing power consumption
  across all channel conditions and process corners. Since this problem is largely
  intractable due to the complexity of simulation across all channel conditions and
  process corners, we propose a self-learning strategy for adaptive MIMO-RF systems.
  In this approach, RF devices learn their own performance vs. power consumption vs.
  tuning knob relationships \"on-the-fly\" and formulate the optimum reconfiguration
  strategy using neural-network based learning techniques during real-time operation.
  The methodology is demonstrated for a MIMO-RF receiver front-end and is supported
  by hardware validation leading to 2.5X power savings in minimal learning time.
publication: '*2014 IEEE/ACM International Conference on Computer-Aided Design (ICCAD)*'
doi: 10.1109/ICCAD.2014.7001430
---
