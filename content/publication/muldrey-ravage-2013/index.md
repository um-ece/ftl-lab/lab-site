---
# Documentation: https://wowchemy.com/docs/managing-content/

title: 'RAVAGE: Post-Silicon Validation of Mixed Signal Systems Using Genetic Stimulus
  Evolution and Model Tuning'
subtitle: ''
summary: ''
authors:
- B. Muldrey
- S. Deyati
- M. Giardino
- A. Chatterjee
tags:
- '"Automatic test pattern generation"'
- '"behavioral model"'
- '"circuit tuning"'
- '"elemental semiconductors"'
- '"genetic algorithms"'
- '"genetic stimulus evolution"'
- '"hardware Trojan detection"'
- '"Integrated circuit modeling"'
- '"integrated circuit testing"'
- '"malicious behaviors"'
- '"Mathematical model"'
- '"mixed analogue-digital integrated circuits"'
- '"mixed signal systems"'
- '"mixed-signal systems-on-chip"'
- '"mixed-signal-RF systems"'
- '"model tuning"'
- '"nonlinear optimization"'
- '"optimisation"'
- '"Post-silicon Validation"'
- '"Radio frequency"'
- '"radiofrequency integrated circuits"'
- '"RAVAGE"'
- '"residual error"'
- '"Si"'
- '"silicon"'
- '"Sociology"'
- '"Statistics"'
- '"stochastic test generation"'
- '"system-on-chip"'
- '"Tuning"'
categories: []
date: '2013-04-01'
lastmod: 2020-10-15T20:25:27-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-10-16T01:25:27.771114Z'
publication_types:
- '1'
abstract: With trends in mixed-signal systems-on-chip indicating increasingly extreme
  scaling of device dimensions and higher levels of integration, the tasks of both
  design and device validation is becoming increasingly complex. Post-silicon validation
  of mixed-signal/RF systems provides assurances of functionality of complex systems
  that cannot be asserted by even some of the most advanced simulators. We introduce
  RAVAGE (from ``random;'' ``validation;'' and ``generation''), an algorithm for generating
  stimuli for post-silicon validation of mixed-signal systems. The approach of RAVAGE
  is new in that no assumption is made about any design anomaly present in the DDT;
  but rather, the stimulus is generated using the DUT itself with the objective of
  maximizing the effects of any behavioral differences between the DUT (hardware)
  and its behavioral model (software) as can be seen in the differences of their response
  to the same stimulus. Stochastic test generation is used since the exact nature
  of any behavioral anomaly in the DUT cannot be known a priori. Once a difference
  is observed, the model parameters are tuned using nonlinear optimization algorithms
  to remove the difference between its and the DUT's responses and the process (test
  generationtextrightarrow tuning) is repeated. If a residual error remains at the
  end of this process that is larger than a predetermined threshold, then it is concluded
  that the DUT contains unknown and possibly malicious behaviors that need further
  investigation. Experimental results on an RF system (hardware) are presented to
  prove feasibility of the proposed technique.
publication: '*VLSI Test Symposium (VTS), 2013 IEEE 31st*'
doi: 10.1109/VTS.2013.6548917
---
