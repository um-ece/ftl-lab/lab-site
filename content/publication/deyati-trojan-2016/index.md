---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Trojan Detection in Digital Systems Using Current Sensing of Pulse Propagation
  in Logic Gates
subtitle: ''
summary: ''
authors:
- S. Deyati
- B. J. Muldrey
- A. Chatterjee
tags:
- '"Capacitance"'
- '"chip manufacturing outsourcing"'
- '"Delays"'
- '"digital systems"'
- '"functional logic testing"'
- '"Hardware"'
- '"Hardware Intrusion Detection"'
- '"Hardware Security"'
- '"hardware Trojan circuit"'
- '"Hardware Trojan Detection"'
- '"high resolution current sensing scheme"'
- '"invasive software"'
- '"Inverters"'
- '"JTAG boundary scan scheme"'
- '"logic circuits"'
- '"logic gates"'
- '"logic testing"'
- '"manufacturing process variation"'
- '"minimal area overhead"'
- '"path delay analysis"'
- '"pulse based Trojan detection scheme"'
- '"pulse measurement"'
- '"pulse propagation sensing"'
- '"sensors"'
- '"Sensors"'
- '"single sensor"'
- '"third party IPs"'
- '"transition probability nodes"'
- '"Trojan horses"'
categories: []
date: '2016-03-01'
lastmod: 2020-10-15T20:25:26-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-10-16T01:25:26.779402Z'
publication_types:
- '1'
abstract: Outsourcing of chip manufacturing to untrusted foundries and using third
  party IPs in design, have opened the possibility of inserting malicious hardware
  Trojans into the circuit. As excitation of Trojan is extremely rare, it is almost
  impossible to detect Trojans with functional logic testing. We need to detect Trojans
  without actually activating it (side channel analysis). Hardware Trojan circuit
  get inputs from low transition probability nodes of the original circuit. Tapping
  of these nodes for creating Trojan inputs increase capacitive load at those nodes.
  We have developed a very high resolution pulse propagation technique to capture
  this extra capacitance at Trojan affected nodes. This technique provides 20-25X
  higher diagnostic resolution than path delay analysis in the presence of significant
  manufacturing process variation. Pulse propagation based Trojan detection is independent
  of logic depth in the path. As the logic depth increases other state of the art
  Trojan detection schemes loses accuracy. Though the scheme appears simple, it is
  not so straight forward to generate and apply the pulse inputs on chip at the desired
  locations and capture them at designated locations with high accuracy in presence
  of high fan out nodes in the design. We have developed a very high resolution current
  sensing scheme to detect pulse propagation through logic gates. A single sensor
  can sense pulse at multiple locations. The entire scheme of pulse based Trojan detection
  has been integrated into JTAG boundary scan scheme with minimal area overhead to
  provide a complete solution for Hardware Trojans.
publication: '*2016 17th International Symposium on Quality Electronic Design (ISQED)*'
doi: 10.1109/ISQED.2016.7479226
---
