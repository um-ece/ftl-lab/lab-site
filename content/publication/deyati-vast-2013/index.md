---
# Documentation: https://wowchemy.com/docs/managing-content/

title: 'VAST: Post-Silicon VAlidation and Diagnosis of RF/Mixed-Signal Circuits Using
  Signature Tests'
subtitle: ''
summary: ''
authors:
- S. Deyati
- A. Banerjee
- B. J. Muldrey
- A. Chatterjee
tags:
- '"behavioral model"'
- '"circuits"'
- '"components"'
- '"components; circuits; devices and systems"'
- '"Computational modeling"'
- '"computing and processing"'
- '"devices and systems"'
- '"DUT"'
- '"fault detection"'
- '"Fault diagnosis"'
- '"fault location"'
- '"fields"'
- '"fields; waves and electromagnetics"'
- '"integrated circuit testing"'
- '"Mathematical model"'
- '"mixed analogue-digital integrated circuits"'
- '"Mixers"'
- '"OFDM transceiver"'
- '"Post silicon validation"'
- '"Post-silicon Validation"'
- '"Radio frequency"'
- '"Receivers"'
- '"reliability"'
- '"RF transceiver"'
- '"RF/mixed-signal circuits"'
- '"signature tests"'
- '"system failure"'
- '"test response signature"'
- '"Transceivers"'
- '"Transmitters"'
- '"VAST"'
- '"verification"'
- '"waves and electromagnetics"'
categories: []
date: '2013-01-01'
lastmod: 2020-10-15T20:25:26-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-10-16T01:25:26.885855Z'
publication_types:
- '1'
abstract: Post-silicon validation of RF/mixed-signal circuits is challenging due to
  the need to excite all possible operational modes of the DUT in order to establish
  equivalence between its specified and observed behaviors and to ensure that the
  DUT does not produce any unexpected behaviors that can lead to system failure. In
  this research, we first develop a methodology for determining if the DUT contains
  behaviors that are not explicitly included in its behavioral model. A complex (optimized)
  test waveform is applied to the DUT and its test response signature is captured.
  It is seen that in the presence of unexpected DUT behaviors, the residual error
  in the test response signature from that defined by the model, cannot be minimized
  below a certain threshold by manipulating the model parameters in any way. If however,
  it is determined that the model is adequate but the signature is different from
  the expected, then a procedure is developed for determining which sub module is
  responsible for the difference (i.e. causes the system level specifications to be
  different from that specified by the behavioral model). Experiments on an RF transceiver
  are performed to demonstrate the effectiveness of the proposed validation and diagnosis
  approach.
publication: '*2013 26th International Conference on VLSI Design and 2013 12th International
  Conference on Embedded Systems*'
doi: 10.1109/VLSID.2013.207
---
