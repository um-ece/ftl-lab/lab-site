---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Targeting Hardware Trojans in Mixed-Signal Circuits for Security
subtitle: ''
summary: ''
authors:
- S. Deyati
- B. J. Muldrey
- A. Chatterjee
tags:
- '"analog circuits"'
- '"analogue integrated circuits"'
- '"Capacitance"'
- '"Delays"'
- '"design-for-Trojan detection"'
- '"Digital circuits"'
- '"digital integrated circuits"'
- '"elemental semiconductors"'
- '"Hardware"'
- '"hardware intrusion detection"'
- '"Hardware security"'
- '"hardware Trojan detection"'
- '"high-resolution detection"'
- '"integrated circuit manufacture"'
- '"Integrated circuits"'
- '"internal circuit nodes"'
- '"invasive software"'
- '"Inverters"'
- '"mixed analogue-digital integrated circuits"'
- '"mixed-signal circuits"'
- '"parasitic loads"'
- '"security"'
- '"Si"'
- '"side channel analysis"'
- '"silicon"'
- '"test stimulus design"'
- '"third-party manufacturing"'
- '"Trojan horses"'
categories: []
date: '2016-07-01'
lastmod: 2020-10-15T20:25:26-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-10-16T01:25:26.412542Z'
publication_types:
- '1'
abstract: The proliferation of third-party silicon manufacturing has increased the
  vulnerability of integrated circuits to malicious insertion of hardware for the
  purpose of leaking secret information or even rendering the circuits useless while
  deployed in the field. A key goal is to detect the presence of such circuits before
  they are activated for subversive reasons. One way to achieve this is to detect
  the presence of parasitic loads on internal nodes of a victim circuit. However,
  such detection becomes difficult in the presence of normal process variations of
  the silicon manufacturing process itself. In this work, we show how high-resolution
  detection of parasitic loads on internal circuit nodes can be achieved using a combination
  of test stimulus design and design-for-Trojan detection techniques. We illustrate
  our ideas on digital as well as analog/mixed-signal circuits and point to directions
  for future research.
publication: '*2016 IEEE 21st International Mixed-Signal Testing Workshop (IMSTW)*'
doi: 10.1109/IMS3TW.2016.7524238
---
