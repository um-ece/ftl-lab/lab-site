---
# Documentation: https://wowchemy.com/docs/managing-content/

title: 'Low Cost Sparse Multiband Signal Characterization Using Asynchronous Multi-Rate
  Sampling: Algorithms and Hardware'
subtitle: ''
summary: ''
authors:
- Nicholas Tzou
- Debesh Bhatta
- Barry J. Muldrey
- Thomas Moon
- Xian Wang
- Hyun Choi
- Abhijit Chatterjee
tags:
- '"Asynchronous multi-rate sampling"'
- '"Low cost spectrum sensing and characterization"'
- '"Sub-Nyquist rate sampling"'
categories: []
date: '2015-01-01'
lastmod: 2020-10-15T20:25:29-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-10-16T01:25:29.292378Z'
publication_types:
- '2'
abstract: Characterizing the spectrum of sparse wideband signals of high-speed devices
  efficiently and precisely is critical in high-speed test instrumentation design.
  Recently proposed sub-Nyquist rate sampling systems have the potential to significantly
  reduce the cost and complexity of sparse spectrum characterization; however, due
  to imperfections and variations in hardware design, numerous implementation and
  calibration issues have risen and need to be solved for robust and stable signal
  acquisition. In this paper, we propose a low-cost and low-complexity hardware architecture
  and associated asynchronous multi-rate sub-Nyquist rate sampling based algorithms
  for sparse spectrum characterization. The proposed scheme can be implemented with
  a single ADC or with multiple ADCs as in multi-channel or band-interleaved sensing
  architectures. Compared to other sub-Nyquist rate sampling methods, the proposed
  hardware scheme can achieve wideband sparse spectrum characterization with minimum
  cost and calibration effort. A hardware prototype built using off-the-shelf components
  is used to demonstrate the feasibility of the proposed approach.
publication: '*Journal of Electronic Testing*'
doi: 10.1007/s10836-015-5505-9
---
