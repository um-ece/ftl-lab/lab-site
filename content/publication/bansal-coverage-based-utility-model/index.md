---
# Documentation: https://wowchemy.com/docs/managing-content/

title: A Coverage-Based Utility Model for Identifying Unknown Unknowns
subtitle: ''
summary: ''
authors:
- Gagan Bansal
- Daniel S Weld
tags: []
categories: []
date: -01-01
lastmod: 2020-10-15T20:25:25-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-10-16T01:25:25.522901Z'
publication_types:
- '2'
abstract: A classifier's low confidence in prediction is often indicative of whether
  its prediction will be wrong; in this case, inputs are called known unknowns. In
  contrast, unknown unknowns (UUs) are inputs on which a classifier makes a high confidence
  mistake. Identifying UUs is especially important in safety-critical domains like
  medicine (diagnosis) and law (recidivism prediction). Previous work by Lakkaraju
  et al. (2017) on identifying unknown unknowns assumes that the utility of each revealed
  UU is independent of the others, rather than considering the set holistically. While
  this assumption yields an efficient discovery algorithm, we argue that it produces
  an incomplete understanding of the classifier's limitations. In response, this paper
  proposes a new class of utility models that rewards how well the discovered UUs
  cover (or ``explain'') a sample distribution of expected queries. Although choosing
  an optimal cover is intractable, even if the UUs were known, our utility model is
  monotone submodular, affording a greedy discovery strategy. Experimental results
  on four datasets show that our method outperforms bandit-based approaches and achieves
  within 60.9% utility of an omniscient, tractable upper bound.
publication: ''
---
