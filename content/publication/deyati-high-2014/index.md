---
# Documentation: https://wowchemy.com/docs/managing-content/

title: 'High Resolution Pulse Propagation Driven Trojan Detection in Digital Logic:
  Optimization Algorithms and Infrastructure'
subtitle: ''
summary: ''
authors:
- Sabyasachi Deyati
- Barry John Muldrey
- Adit Singh
- Abhijit Chatterjee
tags:
- '"Capacitance"'
- '"capacitive loads"'
- '"circuits"'
- '"components"'
- '"components; circuits; devices and systems"'
- '"computing and processing"'
- '"Delays"'
- '"devices and systems"'
- '"diagnostic resolution"'
- '"digital logic"'
- '"Flip-flops"'
- '"Hardware Intrusion Detection"'
- '"Hardware Security"'
- '"Hardware Trojan Detection"'
- '"high-resolution Trojan detection method"'
- '"internal circuit nodes"'
- '"invasive software"'
- '"logic design"'
- '"logic testing"'
- '"malicious circuitry"'
- '"malicious Trojans insertion"'
- '"manufacturing process variations"'
- '"node controllability"'
- '"observability"'
- '"outsourced chip manufacturing"'
- '"path delay measurement"'
- '"pulse propagation"'
- '"side channel Trojan detection techniques"'
- '"Transistors"'
- '"Trojan horses"'
- '"Vectors"'
- '"worst case process variation effects"'
categories: []
date: '2014-01-01'
lastmod: 2020-10-15T20:25:26-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-10-16T01:25:26.271152Z'
publication_types:
- '1'
abstract: Insertion of malicious Trojans into outsourced chip manufacturing generally
  results in increased capacitances of internal circuit nodes that have been tapped
  for node controllability and observability by malicious circuitry. Current path
  delay measurement and side channel Trojan detection techniques are unable to detect
  Trojans that present low loading to such tapped circuit nodes, especially in the
  presence of large manufacturing process variations. In this paper, a high-resolution
  Trojan detection method for digital logic based on pulse propagation is developed.
  The method exhibits 25X -- 30X higher diagnostic resolution (ability to measure
  small capacitive loads on internal circuit nodes) as compared to current path delay
  based Trojan detection techniques in the presence of significant manufacturing process
  variations. Further, a key benefit is that theoretically, as opposed to path delay
  measurement based methods, the diagnostic resolution of the test approach is independent
  of circuit logic depth over and above the benefits already mentioned above. Test
  methods and test infrastructure compatible with existing scan based techniques are
  described. Simulation results are presented to prove the viability and effectiveness
  of the proposed Trojan detection scheme and especially for circuits with large logic
  depths (35-70 gates) suffering from worst case process variation effects.
publication: '*Test Symposium (ATS), 2014 IEEE 23rd Asian*'
doi: 10.1109/ATS.2014.45
---
