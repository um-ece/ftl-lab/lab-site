---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Challenge Engineering and Design of Analog Push Pull Amplifier Based Physically
  Unclonable Function for Hardware Security
subtitle: ''
summary: ''
authors:
- S. Deyati
- B. J. Muldrey
- A. D. Singh
- A. Chatterjee
tags:
- '"analog push pull amplifier"'
- '"arbiter-based digital PUF architectures"'
- '"asynchronous circuits"'
- '"Buildings"'
- '"Clustering algorithms"'
- '"differential amplifier"'
- '"differential amplifiers"'
- '"digital stimulus engineering"'
- '"Hardware"'
- '"hardware security"'
- '"Hardware Security"'
- '"IC signatures"'
- '"IP Protection"'
- '"Manufacturing processes"'
- '"Mathematical model"'
- '"mixed analogue-digital integrated circuits"'
- '"mixed-signal SoC"'
- '"model building attacks"'
- '"physically unclonable function"'
- '"Physically Unclonable Function"'
- '"Reliability"'
- '"security"'
- '"Security"'
- '"silicon authentication"'
- '"system-on-chip"'
- '"systems-on-chips"'
- '"temperature -20 degC to 120 degC"'
- '"transfer function variability"'
- '"transfer functions"'
categories: []
date: '2015-11-01'
lastmod: 2020-10-15T20:25:26-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-10-16T01:25:26.161391Z'
publication_types:
- '1'
abstract: In the recent past, Physically Unclonable Functions (PUFs) have been proposed
  as a way of implementing security in modern ICs. PUFs are hardware designs that
  exploit the randomness in silicon manufacturing processes to create IC-specific
  signatures for silicon authentication. While prior PUF designs have been largely
  digital, in this work we propose a novel PUF design based on transfer function variability
  of an analog push-pull amplifier under process variations. A differential amplifier
  architecture is proposed with digital interfaces to allow the PUF to be used in
  digital as well as mixed-signal SoCs. A key innovation is digital stimulus engineering
  for the analog amplifier that allows 2X improvements in the uniqueness of IC signatures
  generated over arbiter-based digital PUF architectures, while maintaining high signature
  reliability over +/- 10 % voltage and -20 to 120 degree Celsius temperature variation.
  The proposed PUF is also resistive to model building attacks as the internal analog
  operation of the PUF is difficult to reverse-engineer due to the continuum of internal
  states involved. We show the benefits of the proposed PUF through comparison with
  a traditional arbiter-based digital PUF using simulation experiments.
publication: '*2015 IEEE 24th Asian Test Symposium (ATS)*'
doi: 10.1109/ATS.2015.29
---
