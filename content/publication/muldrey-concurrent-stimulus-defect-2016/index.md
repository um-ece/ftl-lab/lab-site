---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Concurrent Stimulus and Defect Magnitude Optimization for Detection of Weakest
  Shorts and Opens in Analog Circuits
subtitle: ''
summary: ''
authors:
- B. Muldrey
- S. Deyati
- A. Chatterjee
tags:
- '"Algorithm design and analysis"'
- '"Analog"'
- '"analog circuits"'
- '"Capacitance"'
- '"Circuit faults"'
- '"Defect detection"'
- '"fault detection"'
- '"Heuristic algorithms"'
- '"Integrated circuit modeling"'
- '"mixed-signal"'
- '"optimization"'
- '"testing"'
- '"Trojans"'
categories: []
date: '2016-11-01'
lastmod: 2020-10-15T20:25:28-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-10-16T01:25:28.166948Z'
publication_types:
- '1'
abstract: We present a methodology for algorithmic generation of test signals for
  thedetection and diagnosis of a variety of short and open-circuit defects in analogcircuits.
  Prior algorithms have focused on test generation for known short oropen defect values.
  This places the burden of failure coverage on accurateanalysis of observed defects
  in known failed parts at high cost. In this work, we optimize the test stimulus
  to detect the it weakest shorts and opens inanalog circuits using a concurrent stimulus
  and defect value optimizationalgorithm. Since the defect value itself is an optimization
  parameter, the responses of nonlinear circuits corresponding to it multipledefect
  values are considered as opposed to a it single linearizedrepresentation corresponding
  to a fixed defect value as in the existing state ofthe art. The algorithm produces
  a test stimulus along with the values of theweakest shorts and opens that the stimulus
  can detect (the locations of thedefects are specified to the algorithm). These values
  are determined by thedesign of the analog circuit itself and therefore subsume all
  specifieddetectable defects for the circuits concerned. Experimental results show
  thefeasibility of the proposed approach on selected test cases and defect sets.
publication: '*2016 IEEE 25th Asian Test Symposium (ATS)*'
doi: 10.1109/ATS.2016.61
---
