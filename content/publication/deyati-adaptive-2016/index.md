---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Adaptive Testing of Analog/RF Circuits Using Hardware Extracted FSM Models
subtitle: ''
summary: ''
authors:
- S. Deyati
- B. J. Muldrey
- A. Chatterjee
tags:
- '"Adaptation models"'
- '"adaptive testing"'
- '"analog-RF circuits"'
- '"analogue integrated circuits"'
- '"Automatic test pattern generation"'
- '"circuit optimisation"'
- '"circuit simulation"'
- '"Circuit testing"'
- '"Classification algorithms"'
- '"Computational modeling"'
- '"Detectors"'
- '"finite state machines"'
- '"Hardware"'
- '"hardware extracted FSM models"'
- '"hardware measurements"'
- '"integrated circuit measurement"'
- '"Integrated circuit modeling"'
- '"integrated circuit testing"'
- '"measurement errors"'
- '"measurement noise"'
- '"measurement uncertainty"'
- '"mixed analogue-digital integrated circuits"'
- '"mixed-signal-RF circuits"'
- '"process perturbations"'
- '"Radio frequency"'
- '"radiofrequency integrated circuits"'
- '"test generation problem"'
- '"test stimulus generation"'
- '"test stimulus optimization"'
- '"tester inaccuracies"'
- '"testing"'
- '"Testing"'
categories: []
date: '2016-04-01'
lastmod: 2020-10-15T20:25:26-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-10-16T01:25:25.950316Z'
publication_types:
- '1'
abstract: The test generation problem for analog/RF circuits has been largely intractable
  due to the fact that repetitive circuit simulation for test stimulus optimization
  is extremely time-consuming. As a consequence, it is difficult, if not impossible,
  to generate tests for practical mixed-signal/RF circuits that include the effects
  of tester inaccuracies and measurement noise. To offset this problem and allow test
  generation to scale to different applications, we propose a new approach in which
  FSM models of mixed-signal/RF circuits are abstracted from hardware measurements
  on fabricated devices. These models allow accurate simulation of device behavior
  under arbitrary stimulus and thereby test stimulus generation, even after the device
  has been shipped to a customer. As a consequence, it becomes possible to detect
  process shifts with fine granularity and regenerate tests to adapt to process perturbations
  in a dynamic manner without losing test accuracy. A complete methodology for such
  adaptive testing of mixed-signal/RF circuits is developed in this paper. Simulation
  results and hardware measurements are used to demonstrate the efficacy of the proposed
  techniques.
publication: '*2016 IEEE 34th VLSI Test Symposium (VTS)*'
doi: 10.1109/VTS.2016.7477283
---
