---
# Display name
title: FTL

# Username (this should match the folder name)
authors:
- ftllab

# Is this the primary user of the site?
superuser: false

# Role/position
role: AI/ML/EDA Lab at Ole Miss ECE

# Organizations/Affiliations
organizations:
- name: University of Mississippi
  url: "https://ece.olemiss.edu"

# Short bio (displayed in user profile at end of posts)
bio: The FTL Lab was born in Oxford, MS in the year 2020.

interests:
    - AMS(+RF) System Validation and Debug
    - Artificial Intelligence
    - Machine Learning
    - EDA/CAD tool development
    - Automated model synthesis for AMS components
    
education:

courses:

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: '#contact'  # For a direct email link, use "mailto:test@example.org".
#- icon: twitter
#  icon_pack: fab
#  link: https://twitter.com/GeorgeCushen
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.com/citations?user=PvOVtwsAAAAJ&hl=en&oi=ao
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.com/um-ece/ftl-lab
- icon: orcid
  icon_pack: ai
  link: https://orcid.org/0000-0003-2052-6096
- icon: dblp
  icon_pack: ai
  link: https://dblp.uni-trier.de/pers/hd/m/Muldrey:Barry_John

# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Highlight the author in author lists? (true/false)
highlight_name: false

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
#- Researchers
#- Visitors
---

The FTL group is the newest research lab in the Ole Miss ECE department.
It began in the fall of 2020, when Barry Muldrey joined the faculty there.

The primary interests of the FTL lab are in the domains of:

#{{% refcheck "pages/research_statement.md" "Statement of Research" %}}
#{{% refcheck "pages/teaching_statement.md" "Teaching Statement" %}}
#{{% refcheck "pages/diversity_statement.md" "Statement on Diversity" %}}
