---
# Display name
title: Dr. Barry Muldrey

# Username (this should match the folder name)
authors:
- bmuldrey

# Is this the primary user of the site?
superuser: true

# Role/position
role: Lab Director and Professor of Electrical and Computer Engineering

# Organizations/Affiliations
organizations:
- name: "Electrical and Computer Engineering,\n University of Mississippi"
  url: "https://ece.olemiss.edu"

# Short bio (displayed in user profile at end of posts)
bio: My research interests include high-level, mixed-signal synthesis and automated model composition.

interests:
- EDA CAD tools
- Artificial Intelligence

education:

courses:
  - course:         "Testing of Computing Systems (Cp E 432/632)"
    institution:    UM ECE
    year:           2020
  - course:         "Senior Design Project (El E 461/462)"
    institution:    UM ECE
    year:           2020

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: '#contact'  # For a direct email link, use "mailto:test@example.org".
#- icon: twitter
#  icon_pack: fab
#  link: https://twitter.com/GeorgeCushen
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.com/citations?user=PvOVtwsAAAAJ&hl=en&oi=ao
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.com/profmuldrey
- icon: orcid
  icon_pack: ai
  link: https://orcid.org/0000-0003-2052-6096
- icon: dblp
  icon_pack: ai
  link: https://dblp.uni-trier.de/pers/hd/m/Muldrey:Barry_John

# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf
- icon: cv
  icon_pack: ai
  link: https://gitlab.com/bjmuld/resume/-/raw/master/muldrey_cv.pdf?inline=true

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "barry@muldrey.net"

# Highlight the author in author lists? (true/false)
highlight_name: false

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Researchers
#- Visitors
---

After attempting a startup in recording studio technology, Barry joined Dr. Abhijit Chatterjee's lab at Georgia Tech and began his pursuit of the Ph.D.
There he conducted research in circuit model validation, debugging, and machine learning for testing analog and mixed-
signal systems, receiving his doctorate in the summer of 2019.

Barry’s other hobbies include architectural and structural design and repair,
fusion jazz music, and woodcraft.

[Request a Meeting (coffee is on me)](https://calendly.com/profmuldrey)

[Barry's Personal Homepage](https://barry.muldrey.net)

{{% refcheck "pages/research_statement.md" "Statement of Research" %}}
{{% refcheck "pages/teaching_statement.md" "Teaching Statement" %}}
{{% refcheck "pages/diversity_statement.md" "Statement on Diversity" %}}
